/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package excepciones;

import java.util.Scanner;


/**
 *
 * @author nacho
 */
public class AppExcepciones {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Calculadora c1=new Calculadora(10,5);
        Calculadora c2=new Calculadora(2,4);
        Calculadora c3=new Calculadora(10,0);
        System.out.println(c1.getOperando1() + " + " + c1.getOperando2() + " = " + c1.suma());
        System.out.println(c1.getOperando1() + " - " + c1.getOperando2() + " = " + c1.resta());
        System.out.println(c1.getOperando1() + " * " + c1.getOperando2() + " = " + c1.producto());
        System.out.println(c1.getOperando1() + " / " + c1.getOperando2() + " = " + c1.cociente());
        System.out.println("");
        System.out.println(c2.getOperando1() + " + " + c2.getOperando2() + " = " + c2.suma());
        System.out.println(c2.getOperando1() + " - " + c2.getOperando2() + " = " + c2.resta());
        System.out.println(c2.getOperando1() + " * " + c2.getOperando2() + " = " + c2.producto());
        System.out.println(c2.getOperando1() + " / " + c2.getOperando2() + " = " + c2.cociente());
        System.out.println("");
        System.out.println(c3.getOperando1() + " + " + c3.getOperando2() + " = " + c3.suma());
        System.out.println(c3.getOperando1() + " - " + c3.getOperando2() + " = " + c3.resta());
        System.out.println(c3.getOperando1() + " * " + c3.getOperando2() + " = " + c3.producto());
        System.out.println(c3.getOperando1() + " / " + c3.getOperando2() + " = " + c3.cociente());   
        
        double d1=10.0;
        double d2=0;
        System.out.println(d1/d2);
        int i1=10;
        //int i2=0;
        Scanner sc = new Scanner(System.in);
        try {
            int i2=sc.nextInt();
            System.out.println(i1/i2);
        } catch (ArithmeticException e) {
            System.out.println(e.getMessage());
            System.out.println("Division por cero!");
        } catch (Exception e) {
            System.out.println("Se ha producido un error");
        }
    }
    
}
