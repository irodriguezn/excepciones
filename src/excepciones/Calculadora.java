/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package excepciones;

/**
 *
 * @author nacho
 */
public class Calculadora {
    private double operando1;
    private double operando2;

    public Calculadora(double operando1, double operando2) {
        this.operando1 = operando1;
        this.operando2 = operando2;
    }

    public double getOperando1() {
        return operando1;
    }

    public void setOperando1(double operando1) {
        this.operando1 = operando1;
    }

    public double getOperando2() {
        return operando2;
    }

    public void setOperando2(double operando2) {
        this.operando2 = operando2;
    }
    
    public double suma() {
        return operando1+operando2;
    }
    
    public double resta() {
        return operando1-operando2;
    }
    
    public double producto() {
        return operando1*operando2-1;
    }
    
    public double cociente() {
        return operando1/operando2;
    }    
    
}
